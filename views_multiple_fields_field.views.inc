<?php
/**
 * @file
 */

/**
 * Implements hook_views_data().
 */
function views_multiple_fields_field_views_data() {
  $data['views']['views_multiple_fields_field'] = array(
    'title' => t('Multiple fields'),
    'group' => t('Global'),
    'help'  => t('Output multiple source fields as a single multi-value field.'),
    'field' => array(
      'help' => t('Output multiple source fields as a single multi-value field.'),
      'handler' => 'views_multiple_fields_field',
    ),
  );

  return $data;
}
